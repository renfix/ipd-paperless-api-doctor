import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';
import { StandingOrderModel } from '../../models/standing-order';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const standingOrderModel = new StandingOrderModel();

  // get standing progress note by group disease id
  // /doctor/standing-progress-note/:group_disease_id/group-disease
  fastify.get('/:group_disease_id/group-disease', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params;
    const groupDiseaseID = params.group_disease_id;

    try {
      const data: any = await standingOrderModel.getByGroupDiseaseID(db, groupDiseaseID);
      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  // get  standing progress note by department id
  // /doctor/standing-progress-note/:department_id/department
  fastify.get('/:department_id/department', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params;
    const departmentID = params.department_id;

    try {
      const data: any = await standingOrderModel.getByDepartmentID(db, departmentID);
      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  // post standing order (create standing-order)
  // /doctor/standing-order
  fastify.post('/', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body;
    const data: any = { ...body }
    
    try {
      const lastID: any = await standingOrderModel.create(db, data);

      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          id: lastID[0].id
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  // put standing order (update standing-order)
  // /doctor/standing-order
  fastify.put('/:standing_order_id', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body;
    const params: any = request.params;
    const standingOrderID = params.standing_order_id;
    const data: any = {...body}

    try {
      const lastID: any = await standingOrderModel.update(db, data, standingOrderID);

      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          id: lastID[0].id
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })
  fastify.get('/list', {
    preHandler: [
      fastify.guard.role(['admin', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const data: any = await standingOrderModel.getList(db);
      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  fastify.get('/getGroupDiseaseByStandingOrder', {
    preHandler: [
      fastify.guard.role(['admin', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const data: any = await standingOrderModel.getGroupDiseaseByStandingOrder(db);
      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })
  done();
}