import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _, { rest } from 'lodash';

// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';
import { StandingProgressNoteModel } from '../../models/standing-progress-note';
import { UUID } from 'crypto';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const standingProgressNoteModel = new StandingProgressNoteModel();

  // get standing progress note
  // /doctor/standing-progress-note
  fastify.get('/', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const _query: any = request.query;
    const { limit, offset, search } = _query;
    const _limit = limit || 20;_
    const _offset = offset || 0;

    try {
      const data: any = await standingProgressNoteModel.getAll(db, search, _limit, _offset);
      const total: any = await standingProgressNoteModel.getTotal(db, search);
      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data,
          total: Number(total.total)
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  // get standing progress note by group disease id
  // /doctor/standing-progress-note/:group_disease_id/group-disease
  fastify.get('/:group_disease_id/group-disease', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params;
    const groupDiseaseID = params.group_disease_id;

    try {
      const data: any = await standingProgressNoteModel.getByGroupDiseaseID(db, groupDiseaseID);
      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  // get  standing progress note by department id
  // /doctor/standing-progress-note/:department_id/department
  fastify.get('/:department_id/department', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params;
    const departmentID = params.department_id;

    try {
      const data: any = await standingProgressNoteModel.getByDepartmentID(db, departmentID);
      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  // post standing progress note (create standing_progress_note)
  // /doctor/standing-progress-note
  fastify.post('/', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body
    const data: any = body

    try {
      const standingProgressNote: any = await standingProgressNoteModel.create(db, data);
      
      // const uuids: UUID[] = standingProgressNote.map((res: any) => {
      //  return res.id
      // })

      const rowsAffected = standingProgressNote.length;

      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          row_affected: rowsAffected
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  // put standing progress note (create standing_progress_note)
  // /doctor/standing-progress-note/:standing_progress_note_id
  fastify.put('/:standing_progress_note_id', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body
    const data: any = body
    const params: any = request.params
    const standingProgressNoteID: UUID = params.standing_progress_note_id

    try {
      const standingProgressNote: any = await standingProgressNoteModel.update(db, data, standingProgressNoteID);
      
      // const uuids: UUID[] = standingProgressNote.map((res: any) => {
      //  return res.id
      // })

      const rowsAffected = standingProgressNote.length;

      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          row_affected: rowsAffected
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })
  fastify.get('/list', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const data: any = await standingProgressNoteModel.getList(db);
      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })


  fastify.get('/getGroupDiseaseByStandingProgressNote', {
    preHandler: [
      fastify.guard.role(['admin', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      const data: any = await standingProgressNoteModel.getGroupDiseaseByStandingProgressNote(db);
      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })
  done();
}