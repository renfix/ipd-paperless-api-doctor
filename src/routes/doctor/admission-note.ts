import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';


// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';
import { AdmissionNoteModel } from '../../models/admission-note';
import { PatientHistoryModel } from '../../models/patient-history'
import { PatientPregnantModel } from '../../models/patient-pregnant'
import { UUID } from 'crypto';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const admissionNoteModel = new AdmissionNoteModel();
  const patientHistoryModel = new PatientHistoryModel();
  const patientPregnantModel = new PatientPregnantModel();

  // put admission note (update admission_note) // get patient info
  // /doctor/admission-note
  fastify.put('/:admit_id', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params
    const admitID: UUID = params.admit_id
    const body: any = request.body;
    const data: any = { ...body }

    try {
      await admissionNoteModel.updateByAdmitID(db, data.admission_note, admitID)
      await patientHistoryModel.updateByAdmitID(db, data.patient_history, admitID)
      await patientPregnantModel.updateByAdmitID(db, data.patient_pregnant, admitID)

      return reply.status(StatusCodes.OK)
        .send({
          status: 'ok',
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  // post admission note (create admission_note) // get patient info
  // /doctor/admission-note
  fastify.post('/', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    const body: any = request.body;
    const data: any = { ...body }
    let rs_info: any = {};

    console.log(data);

    try {
      if (data.admission_note) {
        let rs_admission_note = await admissionNoteModel.upsert(db, data.admission_note)
        console.log('rs_admission_note : ', rs_admission_note);

        rs_info.admission_note = rs_admission_note;
      }

      if (data.patient_history) {
        let rs_patient_history = await patientHistoryModel.upsert(db, data.patient_history)
        console.log('rs_patient_history : ', rs_patient_history);

        rs_info.patient_history = rs_patient_history;
      }

      if (data.patient_pregnant) {
        let rs_patient_pregnant = await patientPregnantModel.upsert(db, data.patient_pregnant)
        console.log('rs_patient_pregnant : ', rs_patient_pregnant);

        rs_info.patient_pregnant = rs_patient_pregnant;
      }

      return reply.status(StatusCodes.CREATED)
        .send({
          status: 'ok', data: rs_info
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.get('/:admit_id', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params;
    const admitID = params.admit_id

    try {
      let admissionNote:any = await admissionNoteModel.getByAdmitID(db, admitID)
      let patientHistory:any = await patientHistoryModel.getByAdmitID(db, admitID)
      let patientPregnant:any = await patientPregnantModel.getByAdmitID(db, admitID)
      
      // let data: any = [];
      if (admissionNote.length > 0) {
        // data.admission_note = admissionNote
        delete admissionNote[0].create_date;
        delete admissionNote[0].create_by;
        // delete admissionNote.modify_date;
        delete admissionNote[0].is_active;
      }
      if (patientHistory.length > 0) {
        delete patientHistory[0].create_date;
        delete patientHistory[0].create_by;
        // delete patientHistory.modify_date;
        delete patientHistory[0].is_active;
      }
      if (patientPregnant.length > 0) {
        delete patientPregnant[0].create_date;
        delete patientPregnant[0].create_by;
        // delete patientPregnant.modify_date;
        delete patientPregnant[0].is_active;
      }

      return reply.status(StatusCodes.OK)
        .send({
          status: 'ok',
          data: { admission_note: admissionNote, patient_history: patientHistory, patient_pregnant: patientPregnant }
          // admission_note: admissionNote,
          // patient_history: patientHistory,
          // patient_pregnant: patientPregnant
        });
    } catch (error) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })
  done();
}