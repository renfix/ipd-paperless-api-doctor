import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { DateTime } from 'luxon';

// schema
import createSchema from '../../schema/admit/create';
import { UUID } from 'crypto';

import { OrderModel } from '../../models/order';
import { DoctorOrderModel } from '../../models/doctor-order';
import { ProgressNoteModel } from '../../models/progress-note';
import { OrderFoodModel } from '../../models/order-food';
import { OrderVitalSignModel } from '../../models/order-vital-sign';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const orderModel = new OrderModel();
  const doctorOrderModel = new DoctorOrderModel();
  const progressNoteModel = new ProgressNoteModel();
  const orderFoodModel = new OrderFoodModel();
  const orderVitalSignModel = new OrderVitalSignModel();

  fastify.get('/:doctor_order_id/doctor-order', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const userID: any = request.user.sub; // user_id from token (payload)
    const params: any = request.params;
    const doctorOrderID: any = params.doctor_order_id
    var data: any = {}

    try {

      const doctorOrder: any = await doctorOrderModel.getByID(db, doctorOrderID)
      const progressNote: any = await progressNoteModel.getByDoctorOrderID(db, doctorOrderID)
      const orders: any = await orderModel.getByDoctorOrderID(db, doctorOrderID)

      data = {
        doctor_order: doctorOrder,
        progress_note: progressNote,
        orders: orders
      }
      reply.status(StatusCodes.OK).send({
        ok: true,
        data
      });
    } catch (error) {
      reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        })
    }

  })

  fastify.post('/', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const userID: any = request.user.sub; // user_id from token (payload)
    const body: any = request.body;
    var data = { ...body }
    let rsInfo:any={};
    try {
      // add doctor_order
      if(data.doctor_order && data.doctor_order[0].id){
        for(let x of data.doctor_order){
          let doctor_order_date = x.doctor_order_date;
          delete x.id;
          delete x.doctor_order_date;
          x.doctor_order_date = DateTime.fromISO(doctor_order_date).toFormat('yyyy-MM-dd');
        }
        let doctorOrder: any = await doctorOrderModel.upsert(db, data.doctor_order);
        rsInfo.doctor_order = await doctorOrder;
        // rsInfo.doctor_order = await data.doctor_order;
        console.log('doctorOrder1111 : ' ,rsInfo.doctor_order);
      }else{
        let doctorOrder: any = await doctorOrderModel.upsert(db, data.doctor_order);
        rsInfo.doctor_order = await doctorOrder;
        console.log('doctorOrder2222 : ' ,rsInfo.doctor_order);
      }
      if (data.doctor_order.length > 0) {
        let doctorOrder: any = await doctorOrderModel.upsert(db, data.doctor_order);
        rsInfo.doctor_order = await doctorOrder;
          // add progress_note
          if (data.progress_note && data.progress_note.length != 0) {
            // console.log('progress_note : ' ,data.progress_note);
            var progressNote: any = await data.progress_note.map((res: any) => {
              return { doctor_order_id: rsInfo.doctor_order[0]?.id, ...res }
            })
            let rs_progressNote:any = await progressNoteModel.upsert(db, progressNote)
            rsInfo.progress_note = await rs_progressNote.rows;
          }
          // add orders
          if (data.orders && data.orders.length != 0) {
            // console.log('orders : ' ,data.orders);
            var orders: any = await data.orders.map((res: any) => {
              return { doctor_order_id: rsInfo.doctor_order[0]?.id, ...res }
            })
            let rs_order:any = await orderModel.upsert(db, orders)
            rsInfo.order = await rs_order;
          }
          // add order_food
          if (data.order_food && data.order_food.length != 0) {
            // console.log('order_food : ' ,data.order_food);
            var orderFood: any = await data.order_food.map((res: any) => {
              return { doctor_order_id: rsInfo.doctor_order[0]?.id, ...res }
            })
            let rs_order_food:any = await orderFoodModel.upsert(db, orderFood)
            rsInfo.order_food = await rs_order_food;
          }
          // add order_vital_sign
          if (data.order_vital_sign && data.order_vital_sign.length != 0) {
            // console.log('order_vital_sign : ' ,data.order_vital_sign);
            var orderVitalSigns: any = await data.order_vital_sign.map((res: any) => {
              return { admit_id: rsInfo.doctor_order[0]?.admit_id, ...res }
            })
            let rs_order_vital_sign:any =  await orderVitalSignModel.upsert(db, orderVitalSigns)
            rsInfo.order_vital_sign = await rs_order_vital_sign;
          }
      }
      reply.status(StatusCodes.OK).send({
        ok: true ,data: rsInfo
      });
    } catch (error) {
      reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        })
    }
  })

  // get doctor order by admit id (admit,doctor_order,orders,progress_note)
  // /doctor/doctor-order/:admit_id
  fastify.get('/:admit_id/admit', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params;
    const admitID: UUID = params.admit_id
    var data: any = []

    try {
      let doctorOrder: any[] = await doctorOrderModel.getByAdmitID(db, admitID)

      await Promise.all(
        doctorOrder.map(async (res: any) => {
          const order: any = await orderModel.getByDoctorOrderID(db, res.id)
          const prgressNote: any = await progressNoteModel.getByDoctorOrderID(db, res.id)
          data.push({ ...res, order, progress_note: prgressNote })
        })
      )

      reply.status(StatusCodes.OK).send({
        ok: true,
        data
      })

    } catch (error) {
      reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        })
    }
  })

  //Delete order
  fastify.delete('/order/:id', {
    preHandler: [
      fastify.guard.role(['admin','doctor','nurse']),
      // fastify.guard.scope(['order.create','admit.read'])
    ],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const id:any = req.params.id;

    try {
      const data:any = await doctorOrderModel.deleteOrder(db,id);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  }) 

  done();
}