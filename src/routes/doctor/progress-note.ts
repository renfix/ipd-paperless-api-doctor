import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';
import { ProgressNoteModel } from '../../models/progress-note';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const progressNoteModel = new ProgressNoteModel();

  // put progress note by doctor order id
  // /doctor/progress-note/:doctor_order_id
  fastify.post('/', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body;
    const data: object = body

    try {
      await progressNoteModel.upsert(db, data)
      return reply.status(StatusCodes.OK).
        send({ ok: true });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  // put progress note by doctor order id
  // /doctor/progress-note/:doctor_order_id
  fastify.put('/:doctor_order_id', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params;
    const doctorOrderID = params.doctor_order_id;
    const body: any = request.body;
    const data: object = body

    try {
      await progressNoteModel.updateByDoctorOrderID(db, data, doctorOrderID)
      return reply.status(StatusCodes.OK).
        send({ ok: true });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  done();
}