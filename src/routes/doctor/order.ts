import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';
import { OrderModel } from '../../models/order';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const orderModel = new OrderModel();

  

  // post admission note (update admission_note) // get patient info
  // /doctor/admission-note
  fastify.post('/', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    
    const body: any = request.body;
    const data: any = body
    
    try {  
      let rs:any = await orderModel.create(db, data)
      return reply.status(StatusCodes.CREATED).send({ ok: true,data: rs });
    } catch (error: any) {
      console.log(error)
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          // error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
          error
        });
    }
  })
  done();
}