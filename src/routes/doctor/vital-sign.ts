import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';
import { UUID } from 'crypto';
import { VitalSignModel } from '../../models/vital-sign';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const vitalSignModel = new VitalSignModel();

  // get view vital sign by admit id
  // /doctor/vital-sign/:admit_id
  fastify.get('/:admit_id', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params
    const admitID: UUID = params.admit_id;
    
    try {
      const data: any = await vitalSignModel.getByAdmitID(db, admitID);
      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  done();
}