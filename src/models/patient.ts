import { Knex } from 'knex';
import { UUID } from 'crypto';

export class PatientModel {

  constructor () { }

  /**
   * 
   * @param db 
   * @param data 
   * @returns sql
   */
  create(db: Knex, data: object) {
    let sql = db('patient')
    .insert(data);

  return sql
  }
  
  /**
   * 
   * @param db 
   * @param data 
   * @param id 
   * @returns sql
   */
  update(db: Knex, data: object, id: UUID) {
    let sql = db('patient')
    .where('id', id)
    .update(data);
    return sql
  }
  
  /**
   * 
   * @param db 
   * @param id 
   * @returns sql
   */
  delete(db: Knex, id: UUID) {
    let sql = db('patient')
    .where('id', id)
    .delete()
    return sql
  }

  getByAdmitID(db: Knex, admitID: UUID) {
    return db('patient')
    .where('admit_id', admitID)
    .andWhere('is_active', true)
      .select('*')
      .first();
  }

  /**
   * 
   * @param db 
   * @param query 
   * @param limit 
   * @param offset 
   * @returns sql
   */
  list(db: Knex, query: any, limit: number, offset: number) {
    let sql = db('patient as p')
    .select('*')
    
    if (query) {
      let _query = `%${query}%`;
      sql.where(builder => {
        builder.whereRaw('LOWER(p.cid)', _query)
          .orWhereRaw('LOWER(p.fname) like LOWER(?)', [_query])
          .orWhereRaw('LOWER(p.lname) like LOWER(?)', [_query])
      })
    }

    return sql.limit(limit).offset(offset);
  }

}