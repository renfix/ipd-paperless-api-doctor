import { Knex } from 'knex';
import { UUID } from 'crypto';

export class DoctorOrderModel {

  create(db: Knex, data: any) {
    return db('doctor_order')
      .insert(data)
  }

  update(db: Knex, data: any, doctorOrderID: UUID) {
    return db('doctor_order')
      .where('id', doctorOrderID)
      .update(data);
  }

  delete(db: Knex, doctorOrderID: UUID) {
    return db('doctor_order')
      .where('id', doctorOrderID)
      .delete();
  }

  getByID(db: Knex, doctorOrderID: UUID) {
    return db('doctor_order')
      .where('id', doctorOrderID)
      .andWhere('is_active', true)
      .select(
        'id', 'admit_id',
        'doctor_order_date', 'doctor_order_time', 'doctor_order_by',
        'is_confirm'
      )
      .first();
  }

  getByAdmitID(db: Knex, admitID: UUID) {
    return db('doctor_order')
      .where('admit_id', admitID)
      .andWhere('is_active', true)
      .select(
        'id', 'admit_id',
        'doctor_order_date', 'doctor_order_time', 'doctor_order_by',
        'is_confirm'
      )
  }
  upsert(db: Knex, data: object) {
    return db('doctor_order')
    .returning('*')
    .insert(data)
    .onConflict(['admit_id', 'doctor_order_date', 'doctor_order_time'])
    .merge()
  }

  deleteOrder(db: Knex, OrderID: UUID) {
    return db('orders')
      .where('id', OrderID)
      .delete();
  }
}