import { Knex } from 'knex';
import { UUID } from 'crypto';

export class WardModel {

  /**
   * 
   * @param db 
   * @param id 
   * @returns 
   */
  getByID(db: Knex, id: UUID) {
    return db('h_ward')
    .where('id', id)
    .select(
      'id', 'name', 'code'
    )
    .first();
  }

  /**
   * 
   * @param db 
   * @param wardIDs 
   * @returns 
   */
  getByIDs(db: Knex, wardIDs: UUID[]) {
    return db('h_ward')
    .whereIn('id', wardIDs)
    .select(
      'id', 'name', 'code'
    )
  }

  getAll(db: Knex, query: any, limit: number, offset: number) {
    let sql = db('h_ward')
    .leftJoin('admit', function() {
      this.on('h_ward.id', 'admit.ward_id')
      .andOn(db.raw('admit.is_active = true'))
    })
    .select('h_ward.id', 'h_ward.name', 'h_ward.bed_amount',db.raw('count(admit.id) as patient_admit'))
    .groupBy('h_ward.id', 'h_ward.name', 'h_ward.bed_amount')
    if (query) {
      let _query = `%${query}%`
      sql.where(builder => {
        builder.whereRaw('LOWER(h_ward.code) like LOWER(?)', [_query])
          .orWhereRaw('LOWER(h_ward.name) like LOWER(?)', [_query])
      })
    }
    return sql.limit(limit).offset(offset)
  }

  getTotalWard(db: Knex, query: any) {
    let sql = db('h_ward')
    .leftJoin('admit', function() {
      this.on('h_ward.id', 'admit.ward_id')
      .andOn(db.raw('admit.is_active = true'))
    })
    .countDistinct('h_ward.id')
    .groupBy('h_ward.id')
    if (query) {
      let _query = `%${query}%`
      sql.where(builder => {
        builder.whereRaw('LOWER(h_ward.code) like LOWER(?)', [_query])
          .orWhereRaw('LOWER(h_ward.name) like LOWER(?)', [_query])
      })
    }
    return sql;
  }
  getWard(db: Knex, query: any, limit: number, offset: number) {
    let sql = db('h_ward')
    .leftJoin('admit', function() {
      this.on('h_ward.id', 'admit.ward_id')
      .andOn(db.raw('admit.is_active = true'))
    })
    .leftJoin('doctor_order','admit.id','doctor_order.admit_id')
    .select('h_ward.id', 'h_ward.name', 'h_ward.bed_amount',
      db.raw('count(distinct (case when admit.is_active = true then admit.id end)) as patient_admit'),
      db.raw('count(distinct (case when doctor_order.doctor_order_date=current_date then doctor_order.id else null end)) as current_doctor_order')
    )
    .groupBy('h_ward.id', 'h_ward.name', 'h_ward.bed_amount')
    if (query) {
      let _query = `%${query}%`
      sql.where(builder => {
        builder.whereRaw('LOWER(h_ward.code) like LOWER(?)', [_query])
        .orWhereRaw('LOWER(h_ward.name) like LOWER(?)', [_query])
      })
    }
    return sql.limit(limit).offset(offset);
  }
  getDoctorRound(db: Knex, date: string) {

      let sql = db('h_ward')
      .leftJoin('admit', function() {
        this.on('h_ward.id', 'admit.ward_id')
        .andOn(db.raw('admit.is_active = true'))
      })
      .leftJoin('doctor_order','admit.id','doctor_order.admit_id')
      .countDistinct('doctor_order.id as total')
      .where('doctor_order.doctor_order_date', date )
      .groupBy('h_ward.id')
      return sql;
    }

  // getAdmitByWardID(db: Knex, wardID: UUID) {
  //   let sql = db('admit')
  //   .leftJoin('patient','admit.an', 'patient.an')
  //   .leftJoin('profile', 'admit.doctor_id','profile.user_id')
  //   .select(
  //     'admit.id as admit_id','admit.an', 'admit.hn', 
  //     'profile.title','profile.fname','profile.lname',
  //   db.raw('count(admit.id) as patient_admit'))
  //   .groupBy('admit.id','admit.an','admit.hn', 'profile.fname', 'profile.lname', 'profile.title')
  //   return sql
  // }

}



