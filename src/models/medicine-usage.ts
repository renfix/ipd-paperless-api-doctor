import { Knex } from 'knex';
import { UUID } from 'crypto';

export class MedicineUsageModel {

  constructor () { }

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  create(db: Knex, data: object) {
    return db('h_medicine_usage')
      .returning('id')
      .insert(data);
  }

  /**
   * 
   * @param db 
   * @param data 
   * @param medicineUsageID 
   * @returns 
   */
  update(db: Knex, data: object, medicineUsageID: UUID) {
    return db('h_medicine_usage')
      .returning('id')
      .where('id', medicineUsageID)
      .update(data)
  }

  /**
   * 
   * @param db 
   * @param medicineUsageID 
   * @returns 
   */
  delete(db: Knex, medicineUsageID: UUID) {
    return db('h_medicine_usage')
      .returning('id')
      .where('id', medicineUsageID)
      .delete();
  }

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  upsert(db: Knex, data: any) {
    return db('h_medicine_usage')
      .insert(data)
      .onConflict('id')
      .merge();
  }

  getByID(db: Knex, medicineUsageID: UUID) {
    return db('h_medicine_usage')
    .where('id', medicineUsageID)
    .andWhere('is_active', true)
    .select('id', 'code', 'name', 'description', 'time_per_day')
  }

  getAll(db: Knex, search: any, limit: number, offset: number) {
    let sql = db('h_medicine_usage')
    .select(
      'id', 'code', 'name', 'description', 'time_per_day'
    )
    
    
    if (search) {
      let query = `%${search}%`
      sql.where(builder => {
        builder.whereRaw('code like ?', [query])
          .orWhereRaw('name like ?', [query])
          .orWhereRaw('description like ?', [query])
      })
    }

    return sql.limit(limit).offset(offset);
  }

  getTotal(db: Knex, search: any) {
    let sql = db('h_medicine_usage')

    if (search) {
      let query = `%${search}%`
      sql.where(builder => {
        builder.whereRaw('code like ?', [query])
          .orWhereRaw('name like ?', [query])
          .orWhereRaw('description like ?', [query])
      })
    }

    return sql
      .count({ total: '*' }).first();
  }
};
